from django.conf.urls import url

from . import views


urlpatterns = [
#    url(r'index', views.index),
    url(r'post', views.post),
    url(r'^get/(?P<code>[0-9]+)/0', views.get1),
    url(r'^get/(?P<code>[0-9]+)/1', views.get2),
    url(r'^get/(?P<code>[0-9]+)/2', views.get3),
    url(r'^add', views.add),
    url(r'^feedback1/(?P<code>[0-9]+)/', views.feedback1),
    url(r'^feedback2/(?P<code>[0-9]+)/', views.feedback2),
    url(r'^feedback3/(?P<code>[0-9]+)/', views.feedback3),
]