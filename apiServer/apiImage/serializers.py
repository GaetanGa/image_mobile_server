from rest_framework import serializers
from PIL import Image


class ImageSerializer(serializers.ModelSerializer):

    thumbnail = serializers.ImageField(use_url=True)
    source = serializers.ImageField(use_url=True)
    class Meta:
        model = Image
        fields = "__all__"