import os
import numpy as np
import h5py
import matplotlib.pyplot as plt
import matplotlib.image as mpimg

from .extract_cnn_vgg16_keras import VGGNet


def imgSearch(queryDir):

    # read in indexed images' feature vectors and corresponding image names

    h5f = h5py.File('D:/gaeta/Documents/Cours/IMT/image_mobile/project/image_mobile_server/apiServer/apiImage/CNN/featureCNN.h5', 'r')
    feats = h5f['dataset_feat'][:]
    imgNames = h5f['dataset_name'][:]
    h5f.close()

    print ("--------------------------------------------------")
    print ("               searching starts")
    print ("--------------------------------------------------")

    # read and show query image
    queryImg = mpimg.imread(queryDir)
    plt.figure()
    plt.subplot(2, 1, 1)
    plt.imshow(queryImg)
    plt.title("Query Image")
    plt.axis('off')

    # init VGGNet16 model
    model = VGGNet()

    # extract query image's feature, compute simlarity score and sort
    queryVec = model.extract_feat(queryDir)
    scores = np.dot(queryVec, feats.T)
    rank_ID = np.argsort(scores)[::-1]


    # number of top retrieved images to show
    maxres = 3
    imlist = [imgNames[index] for i,index in enumerate(rank_ID[0:maxres])]
    print ("top %d images in order are: " %maxres, imlist)

    return imlist