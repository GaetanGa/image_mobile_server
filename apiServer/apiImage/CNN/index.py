
import os
import h5py
import numpy as np

from apiServer.apiImage.CNN.extract_cnn_vgg16_keras import VGGNet


######################################################################
# THIS FUNCTION PUT THE PATH OF ALL PICTURES IN A LIST AND RETURN IT #
######################################################################
def config_db(dbPath):
    fichier = []
    for root, dirs, files in os.walk(dbPath):
        for i in files:
            if i.endswith('.jpg'):
                fichier.append(os.path.join(root, i))
    return fichier

#def get_imlist(path):  --> ORGINAL
#    return [os.path.join(path,f) for f in os.listdir(path) if f.endswith('.jpg')]  --> ORGINAL

if __name__ == "__main__":

    #db = img_paths = '../data/Img'  --> ORGINAL
    #img_list = get_imlist(db)  --> ORGINAL
    img_list = config_db('D:\\gaeta\\Documents\\Cours\\IMT\\image_mobile\\project\\image_mobile_server\\data\\Img')
    
    print ("--------------------------------------------------")
    print ("         feature extraction starts")
    print ("--------------------------------------------------")
    
    feats = []
    names = []

    model = VGGNet()
    for i, img_path in enumerate(img_list):
        norm_feat = model.extract_feat(img_path)
        #img_name = os.path.split(img_path)[1]
        feats.append(norm_feat)
        names.append(img_path)
        print ("extracting feature from image No. %d , %d images in total" %((i+1), len(img_list)))

    feats = np.array(feats)
    output = 'featureCNN.h5'
    
    print ("--------------------------------------------------")
    print ("      writing feature extraction results ...")
    print ("--------------------------------------------------")
    
    h5f = h5py.File(output, 'w')
    h5f.create_dataset('dataset_feat', data=feats)
    names = [name.encode('utf8') for name in names]
    h5f.create_dataset('dataset_name', data=names)
    h5f.close()
