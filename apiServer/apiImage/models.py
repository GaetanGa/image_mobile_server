from django.db import models

#Stockage de la recherche pour chaque image
class Image(models.Model):
    code = models.DecimalField(max_digits=30, decimal_places=2, default=00000)
    uri1 = models.CharField(max_length=200, default='')
    uri2 = models.CharField(max_length=200, default='')
    uri3 = models.CharField(max_length=200, default='')

    def __str__(self):
        return self.uri1

    def getUris(self):
        uris = [self.uri1, self.uri2, self.uri3]
        return uris

#Stockage de chaque image de DB qui on étaient fournis en résultat
class ImagesAdded(models.Model):
    code = models.DecimalField(max_digits=30, decimal_places=2, default=00000)
    uri = models.CharField(max_length=200, default='')
    mail = models.CharField(max_length=200, default='')
    commentaire = models.CharField(max_length=200, default='')

    def __str__(self):
        return "uri : " + self.uri + "  mail : " + self.mail + "  commentaire : " + self.commentaire

#Enregistrement du feedback de chage image
class Feedback(models.Model):
    uri = models.CharField(max_length=200, default='')
    relevant = models.DecimalField(max_digits=30, decimal_places=2, default=00000)
    noRelevant = models.DecimalField(max_digits=30, decimal_places=2, default=00000)

    def setRelevant(self):
        self.relevant = self.relevant +1

    def setNotRelevant(self):
        self.noRelevant = self.noRelevant +1

    def getRelevant(self):
        return str(self.relevant)

    def getNoRelevant(self):
        return str(self.noRelevant)

    def __str__(self):
        return "URI : " + self.uri + ".   " + "Relevant : " + str(self.relevant) + ". Not relevant : " + str(self.noRelevant)
