#python manage.py runserver 192.168.0.45:8000 --nothreading

import ast
import json
import binascii
import struct
from django import forms
from django.core.files.storage import FileSystemStorage
from django.http import HttpResponse
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response
from rest_framework import status
from django.views.decorators.csrf import csrf_exempt
from .models import Image, ImagesAdded, Feedback
from random import randrange

import io
import PIL.Image as ImagePIL

from .CNN.query_online import imgSearch

PATH ="D:\\gaeta\\Documents\\Cours\\IMT\\image_mobile\\project\\image_mobile_server\\data\\img_uploaded"
PATH2 = "D:\\gaeta\\Documents\\Cours\\IMT\\image_mobile\\project\\image_mobile_server\\data\\\Img\\Added_by_user\\"

#Page d'accueille
def index(request):
    message = "Bienvenue sur la page des API rest"
    return HttpResponse(message)

#Fonction de recherche d'image à partir du POST du client
@csrf_exempt
def post(request):

    if request.method == 'POST':

        codesUsed = [image.code for image in Image.objects.all()]
        value = randrange(10000, 99999)

        while codesUsed.__contains__(value) == True:
            value = randrange(10000, 99999)

        image = ImagePIL.open(io.BytesIO(request.body))
        fs = FileSystemStorage(location=PATH)
        #ImagePIL.Path.replace(Image, PATH+"\loremIpsum.jpeg")
        #filename = fs.save(image.name, image)
        image.save(PATH +"\\"+str(value)+".jpeg")
        uploaded_file_url = 'D:\\gaeta\\Documents\\Cours\\IMT\\image_mobile\\project\\image_mobile_server\\data\\img_uploaded\\' + fs.url(str(value)+".jpeg")

        result1 = imgSearch(uploaded_file_url)[0]
        result2 = imgSearch(uploaded_file_url)[1]
        result3 = imgSearch(uploaded_file_url)[2]

        Image.objects.create(code=value, uri1=result1, uri2=result2, uri3=result3)

        num_result1 = Feedback.objects.filter(uri=result1).count()
        num_result2 = Feedback.objects.filter(uri=result2).count()
        num_result3 = Feedback.objects.filter(uri=result3).count()
        print(num_result1)
        print(num_result2)
        print(num_result3)

        print(Feedback.objects.filter(uri=result1))

        if num_result1 == 0:
            Feedback.objects.create(uri=result1)
            print("yaaaaaaaa !!!!!!")

        if num_result2 == 0:
            Feedback.objects.create(uri=result2)
            print("yaaaaaaaa2 !!!!!!")

        if num_result3 == 0:
            Feedback.objects.create(uri=result3)
            print("yaaaaaaaa3 !!!!!!")

        feed1 = Feedback.objects.get(uri=result1)
        feed2 = Feedback.objects.get(uri=result2)
        feed3 = Feedback.objects.get(uri=result3)

        relevant1 = feed1.getRelevant()
        non_relevant1 = feed1.getNoRelevant()
        relevant2 = feed2.getRelevant()
        non_relevant2 = feed2.getNoRelevant()
        relevant3 = feed3.getRelevant()
        non_relevant3 = feed3.getNoRelevant()

        print(relevant1 + relevant2 + relevant3 + non_relevant1 + non_relevant2 + non_relevant3)

        response = Response(
            {"Ressource_name": value,
             "relevant1": relevant1,
             "non_relevant1": non_relevant1,
             "relevant2": relevant2,
             "non_relevant2": non_relevant2,
             "relevant3": relevant3,
             "non_relevant3": non_relevant3},
            content_type="application/json",
            status=status.HTTP_201_CREATED,
        )
        response.accepted_renderer = JSONRenderer()
        response.accepted_media_type = "application/json"
        response.renderer_context = {}
        return response

#Get la première image du resultat de la recherche d'image suite au POST
@csrf_exempt
def get1(request, code):

    if request.method == 'GET':
        imageObject = Image.objects.get(code=code)
        uri = Image.getUris(imageObject)

        uri = uri[0].split('\'')[1]
        image_data = open(uri, "rb").read()

        return HttpResponse(image_data, content_type="image/png")

    return HttpResponse("Not a GET request")

#Get la seconde image du resultat de la recherche d'image suite au POST
@csrf_exempt
def get2(request, code):

    if request.method == 'GET':
        imageObject = Image.objects.get(code=code)
        uri = Image.getUris(imageObject)

        uri = uri[1].split('\'')[1]
        image_data = open(uri, "rb").read()

        return HttpResponse(image_data, content_type="image/png")

    return HttpResponse("Not a GET request")

#Get la troisième image du resultat de la recherche d'image suite au POST
def get3(request, code):

    if request.method == 'GET':
        imageObject = Image.objects.get(code=code)
        uri = Image.getUris(imageObject)

        uri = uri[2].split('\'')[1]
        image_data = open(uri, "rb").read()

        return HttpResponse(image_data, content_type="image/png")

    return HttpResponse("Not a GET request")

#Ajout d'une image avec le commentaire et l'adresse email
@csrf_exempt
def add(request):

    if request.method == 'POST':

        body_unicode = request.body.decode('utf-8', 'ignore')
        body = json.loads(body_unicode)
        mail = body['mail']
        commentaire = body['commentaire']
        imageSent = body['image']
        imageSent = binascii.a2b_base64(imageSent)



        codesUsed = [image.code for image in ImagesAdded.objects.all()]
        value = randrange(10000, 99999)
        while codesUsed.__contains__(value) == True:
            value = randrange(10000, 99999)


        image = ImagePIL.open(io.BytesIO(imageSent))
        fs = FileSystemStorage(location=PATH)
        image.save(PATH2+str(value)+".jpeg")
        uploaded_file_url = PATH2 + fs.url(str(value)+".jpeg")

        lui = ImagesAdded.objects.create(code=value, uri=uploaded_file_url, mail=mail, commentaire=commentaire)
        print(ImagesAdded.__str__(lui))

        response = HttpResponse(value)
        return response

#Enregistrement du feedback du client pour la première image
@csrf_exempt
def feedback1(request, code):
    if request.method == 'POST':

        body_unicode = request.body.decode('utf-8', 'ignore')
        body = json.loads(body_unicode)
        received_json_data = body['result']

        requestObject = Image.objects.get(code=code)

        uri1 = Image.getUris(requestObject)
        uri1 = uri1[0].split('\'')[1]

        if(received_json_data == 1):
            obj = Feedback.objects.get(uri=("b'" + uri1+"'"))
            Feedback.setRelevant(obj)
            obj.save()
            result = Feedback.getRelevant(obj)

        if (received_json_data == 0):
            obj = Feedback.objects.get(uri=("b'" + uri1+"'"))
            Feedback.setNotRelevant(obj)
            obj.save()
            result = Feedback.getNoRelevant(obj)

        result = Feedback.getRelevant(Feedback.objects.get(uri=("b'" + uri1+"'")))
        print(result)

    return HttpResponse("Saved")

#Enregistrement du feedback du client pour la seconde image
@csrf_exempt
def feedback2(request, code):
    if request.method == 'POST':

        body_unicode = request.body.decode('utf-8', 'ignore')
        body = json.loads(body_unicode)
        received_json_data = body['result']

        requestObject = Image.objects.get(code=code)

        uri2 = Image.getUris(requestObject)
        uri2 = uri2[1].split('\'')[1]

        if (received_json_data == 1):
            obj = Feedback.objects.get(uri=("b'" + uri2 + "'"))
            Feedback.setRelevant(obj)
            obj.save()
            result = Feedback.getRelevant(obj)

        if (received_json_data == 0):
            obj = Feedback.objects.get(uri=("b'" + uri2+"'"))
            Feedback.setNotRelevant(obj)
            obj.save()
            result = Feedback.getNoRelevant(obj)

        print(result)

    return HttpResponse("Saved")

#Enregistrement du feedback du client pour la troisième image
@csrf_exempt
def feedback3(request, code):
    if request.method == 'POST':

        body_unicode = request.body.decode('utf-8', 'ignore')
        body = json.loads(body_unicode)
        received_json_data = body['result']

        requestObject = Image.objects.get(code=code)

        uri3 = Image.getUris(requestObject)
        uri3 = uri3[2].split('\'')[1]

        if(received_json_data == 1):
            obj = Feedback.objects.get(uri=("b'" + uri3+"'"))
            Feedback.setRelevant(obj)
            obj.save()
            result = Feedback.getNoRelevant(obj)

        if (received_json_data == 0):
            obj = Feedback.objects.get(uri=("b'" + uri3 + "'"))
            Feedback.setNotRelevant(obj)
            obj.save()
            result = Feedback.getNoRelevant(obj)

        print(result)

    return HttpResponse("Saved")